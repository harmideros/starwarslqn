# Prueba de conocimiento Python / Django / Graphene  (Graphql)

# Objetivo:
Desarrollar un API GraphQL para un sitio web para los fanáticos de Star Wars.

# Descripción
La aplicación debe estar escrita con Python y el framework Django debe permitir al usuario
ver un listado de todos los personajes relacionadas con el universo de star wars, cada
personaje debe permitir ver las películas en las que dicho personaje participa, cada película
debe tener un detalle donde se muestre el texto apertura, los planetas que se muestran en
cada película, el director y los productores (y los datos que usted considere relevantes).

Consideraciones:
1. Crear Query para listar personas, este query me debe permitir filtrar por nombre de
personas.
2. Diseñar las mutacion que me permita agregar personajes, planetas, películas.
3. Desarrollar pruebas unitarias que me permita testear las mutation y queries.
4. Subir a github.


# Para correr el projecto

Consideraciones: El entorno virtual se creo para un entorno Linux

1. `$ git clone https://gitlab.com/harmideros/starwarslqn.git`
2. `$ cd starwarslqn`
3. `$ source env/bin/activate`
4. `(env) $ python manage.py runserver`

# Navega hasta localhost:8000/graphql/
Si todo salió bien, solo basta con hacer las pruebas para soportar el requerimiento

Para acceder a esta url se requiere que el usuario este autenticado con el sistema de autenticación básica de Django, 
por cuanto el sistema va a hacer una redirección al sitio de login si aun no se encuentra autenticado algun usuario.

El usuario de prueba en el sistema es:

`user: user@test.com`
`pass: test123`

# Para crear un nuevo usuario administrador

` $ python manage.py createsuperuser`

Debe rellenar la información que se le solicita a continuación para crear un nuevo usuario.



# Si el proyecto no corre
Probablemente el entorno sobre el que se corre el proyecto no es un entorno linux, de manera que se debe
generar nuevamente el entorno virtual para el sistema operativo (Windows).

Consideraciones: Python debe estar instalado previamente en el sistema operativo y configurado correctamente.

1. `$ git clone https://gitlab.com/harmideros/starwarslqn.git`
2. `$ cd starwarslqn`
3. Elimina la carpeta env/
4. `> python -m venv env`
5. `> env\Scripts\Activate.bat`
6. `(env)> python manage.py runserver`