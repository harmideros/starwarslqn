import graphene
from graphene import relay, ObjectType
from graphene_django.filter import DjangoFilterConnectionField
from core.nodes import FilmNode, PlanetNode, PersonNode
from core.mutations import CreateFilmMutation, CreatePlanetMutation, CreatePersonMutation 


class Query(graphene.ObjectType):
    planet        = relay.Node.Field(PlanetNode)
    all_planets   = DjangoFilterConnectionField(PlanetNode, description="Get all information of planets")

    person        = relay.Node.Field(PersonNode)
    all_persons   = DjangoFilterConnectionField(PersonNode, description="Get all information of planets, it can be filtered by name")

    film          = relay.Node.Field(FilmNode)
    all_films     = DjangoFilterConnectionField(FilmNode, description="Get all information of planets")


class Mutation(graphene.ObjectType):
    create_person = CreatePersonMutation.Field()
    create_planet = CreatePlanetMutation.Field()
    create_film   = CreateFilmMutation.Field()