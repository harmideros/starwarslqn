import graphene

class PlanetsInput(graphene.InputObjectType):
    name            = graphene.String(required=True)
    gravity         = graphene.String()
    orbital_period  = graphene.Int()