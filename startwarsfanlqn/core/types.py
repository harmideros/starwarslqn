from core import models
from graphene_django import DjangoObjectType


class PersonType(DjangoObjectType):
    class Meta:
        model = models.Person
        description = "Fields available to create a Person object"

class PlanetType(DjangoObjectType):
    class Meta:
        model = models.Planet
        description = "Fields available to create a Planet object"

class FilmType(DjangoObjectType):
    class Meta:
        model = models.Film
        description = "Fields available to create a Film object"