from core import models
from graphene import relay
from graphene_django import DjangoObjectType

class PlanetNode(DjangoObjectType):
    """
    Get Planet info By ID
    """
    class Meta:
        model = models.Planet
        filter_fields = []
        interfaces = (relay.Node, )
        
class PersonNode(DjangoObjectType):
    """
    Get Person info By ID
    """
    class Meta:
        model = models.Person
        filter_fields = {
            'name': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node, )

class FilmNode(DjangoObjectType):
    """
    Get Film info By ID
    """

    class Meta:
        model = models.Film
        filter_fields = []
        interfaces = (relay.Node, )