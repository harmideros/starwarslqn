from django.db import models

# Create your models here.
class Planet(models.Model):
    name = models.CharField(max_length=200)
    gravity = models.CharField(max_length=200, null=True)
    orbital_period = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now=True)

class Person(models.Model):
    name = models.CharField(max_length=200)
    gender = models.CharField(max_length=2, choices=(('M', 'Male'), ('F', "Female"), ('U','Unknown'), ('H', 'Hermaphrodite')))
    created_at = models.DateTimeField(auto_now=True)
    films = models.ManyToManyField('Film')

class Film(models.Model):
    title = models.CharField(max_length=255)
    opening_crawl = models.TextField()
    planets = models.ManyToManyField('Planet')
    director = models.CharField(max_length=255)
    producers = models.CharField(max_length=255)