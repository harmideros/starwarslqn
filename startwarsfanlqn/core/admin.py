from django.contrib import admin
from .models import Film, Person, Planet

admin.site.register(Film)
admin.site.register(Person)
admin.site.register(Planet)
