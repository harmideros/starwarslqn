# from django.test import TestCase

# Create your tests here.
import json

from graphene_django.utils.testing import GraphQLTestCase
from startwarsfanlqn_settings.schema import schema
from unittest import mock
from graphene_django.views import GraphQLView

class CoreAppTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema


    def test_authenticated_user(self):
        """
        Return: 302 Found, If the user is unauthenticated
        """
        response = self.query(
            '''
            mutation createPerson {
                createPerson (input: $input) {
                    person {
                        id
                    }
                }
            }
            ''',
            op_name='createPerson',
            input_data={'name': 'New Person', 'gender': 'M'}
        )

        self.assertEqual(response.status_code, 302)


    # @mock.patch('django.contrib.auth.mixins.LoginRequiredMixin.dispatch')
    # def test_get_all_personsy(self, auth_mock):
    #     """
    #         Response: 200 ok, List of persons with Films asociated, an related
    #     """
    #     response = self.query(
    #         '''
    #         query getPersons {
    #             allPersons{
    #                 edges{
    #                 node{
    #                     name
    #                     gender  
    #                     films {
    #                     title
    #                     planets{
    #                         name
    #                     }
    #                     }
    #                 }
    #                 }
    #             }
    #         }
    #         ''',
    #         op_name='getPersons'
    #     )

    #     content = response.content
    #     self.assertResponseNoErrors(response)



    # @mock.patch('django.contrib.auth.mixins.LoginRequiredMixin.dispatch')
    # def test_create_person_successfull(self, auth_mock):
    #     """
    #         Return: 201 Created, If a person was created successfuly
    #     """
    #     auth_mock.return_value = None
    #     response = self.query(
    #         '''
    #         mutation createPerson {
    #             createPerson (input: $input) {
    #                 person {
    #                     id
    #                     name
    #                     gender
    #                 }
    #             }
    #         }
    #         ''',
    #         op_name='createPerson',
    #         input_data={'name': 'New Person', 'gender': 'M'}
    #     )

    #     # This validates the status code and if you get errors
    #     json_response = json.loads(response.content)
    #     self.assertResponseNoErrors(response)
    #     self.assertEquals(json_response["data"]["createPerson"]["person"]["name"], 'New Person')
    #     self.assertEquals(json_response["data"]["createPerson"]["person"]["gender"], 'M')

    
    # @mock.patch('django.contrib.auth.mixins.LoginRequiredMixin.dispatch')
    # def test_create_person_invalid_arguments(self, auth_mock):
    #     """
    #         Return: 201 Created, If a person was created successfuly
    #     """
    #     auth_mock.return_value = None
    #     response = self.query(
    #         '''
    #         mutation createPerson {
    #             createPerson (input: $input) {
    #                 person {
    #                     id
    #                     name
    #                     gender
    #                 }
    #             }
    #         }
    #         ''',
    #         op_name='createPerson',
    #         input_data={'name': 'A'*300, 'gender': 'asdf'}
    #     )

    #     # This validates the status code and if you get errors
    #     json_response = json.loads(response.content)
    #     self.assertResponseNoErrors(response)
    #     self.assertEquals(json_response["data"]["createPerson"]["person"]["name"], 'higher than 250')
    #     self.assertEquals(json_response["data"]["createPerson"]["person"]["gender"], "'asdf' is not a valid choice")