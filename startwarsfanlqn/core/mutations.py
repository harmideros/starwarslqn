import graphene
from core import models
from core.types import PersonType, PlanetType, FilmType
from core.inputs import PlanetsInput


class CreatePersonMutation(graphene.Mutation):
    """
    Create a Person
    """
    class Arguments:
        name    = graphene.String(required=True) 
        gender  = graphene.String(required=True)

    person = graphene.Field(PersonType)

    def mutate(self, info, name, gender):
        person = models.Person.objects.create(name=name, gender=gender)
        return CreatePersonMutation(person=person)


class CreatePlanetMutation(graphene.Mutation):
    """
    Create a planet
    """
    class Arguments:
        name            = graphene.String(required=True)
        gravity         = graphene.String()
        orbital_period  = graphene.Int()

    planet = graphene.Field(PlanetType)

    def mutate(self, info, name, gravity=None, orbital_period=None):
        planet = models.Planet.objects.create(name=name, gravity=gravity, orbital_period=orbital_period)
        return CreatePlanetMutation(planet=planet)


class CreateFilmMutation(graphene.Mutation):
    """
    Save a Film, it can be save optionaly planets related to Film
    """
    class Arguments:
        title           = graphene.String(required=True)
        opening_crawl   = graphene.String(required=True)
        director        = graphene.String(required=True)
        producers       = graphene.String(required=True)
        planets         = graphene.List(PlanetsInput)

    film = graphene.Field(FilmType)

    def mutate(self, info, title, opening_crawl, director, producers, planets=None):
        film            = models.Film.objects.create(title=title,
                                                     opening_crawl=opening_crawl,
                                                     director=director,
                                                     producers=producers)

        if planets:
            for planet in planets:
                new_planet = models.Planet.objects.create(**planet)
                new_planet.save()
                film.planets.add(new_planet)
            film.save()

        return CreateFilmMutation(film=film)