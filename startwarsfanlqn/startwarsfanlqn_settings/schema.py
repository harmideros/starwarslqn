import graphene
import core.schema


class Query(core.schema.Query, graphene.ObjectType):
    """
    Available Methods to get information about of Films, Persons and Planets in Core app
    """

class Mutation(core.schema.Mutation, graphene.ObjectType):
    """
    Available Methods to create data about of Films, Persons and Planets
    """

schema = graphene.Schema(query=Query, mutation=Mutation)